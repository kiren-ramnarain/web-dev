INSERT INTO USER (ID,USERNAME,PASSWORD,ROLE) VALUES (1,'admin','$2a$10$Jph9vaG2MvL2QsiCKJGxXeonLXbjlOntxNotb24jnet0AHCc8TTLW','ADMIN');
INSERT INTO USER (ID,USERNAME,PASSWORD,ROLE) VALUES (2,'paday','$2a$10$VnpOw0Ng8GrjR7foRZDY5.io7pt.0VTEaLQvwg96bZF1vWJw9Jinq','DOCTOR');

INSERT INTO PATIENT (id,full_Name,contact_Number,email,adm_Date,doctor,member_No,medical_Aid_Member) VALUES (1,'Solomon','16203009658','sapien.Nunc@atpretium.ca',CURRENT_DATE ,'Cheryl','1614052116499','true');
INSERT INTO PATIENT (id,full_Name,contact_Number,email,adm_Date,doctor,member_No,medical_Aid_Member) VALUES (2,'Seth','12237207464','metus@nisi.com',CURRENT_DATE,'Kalia','1601070927499','true');
INSERT INTO PATIENT (id,full_Name,contact_Number,email,adm_Date,doctor,member_No,medical_Aid_Member) VALUES (3,'Cedric','14313711403','urna.suscipit@nec.ca',CURRENT_DATE,'Idola','1626031794199','true');
INSERT INTO PATIENT (id,full_Name,contact_Number,email,adm_Date,doctor,member_No,medical_Aid_Member) VALUES (4,'Gage','17865157216','justo.Proin.non@mauris.org',CURRENT_DATE,'Deirdre','1639071870699','true');
INSERT INTO PATIENT (id,full_Name,contact_Number,email,adm_Date,doctor,member_No,medical_Aid_Member) VALUES (5,'Cade','12433652219','a@ad.net',CURRENT_DATE,'Blossom','1667092096299','true');
INSERT INTO PATIENT (id,full_Name,contact_Number,email,adm_Date,doctor,member_No,medical_Aid_Member) VALUES (6,'Jonas','16352961469','tristique@cursus.org',CURRENT_DATE,'Quin','1694112550699','true');
INSERT INTO PATIENT (id,full_Name,contact_Number,email,adm_Date,doctor,member_No,medical_Aid_Member) VALUES (7,'Deacon','11409753177','adipiscing@enim.com',CURRENT_DATE,'Iris','1628020433799','true');
INSERT INTO PATIENT (id,full_Name,contact_Number,email,adm_Date,doctor,member_No,medical_Aid_Member) VALUES (8,'Zahir','18381451264','Aliquam.fringilla.cursus@malesuadaaugue.com',CURRENT_DATE,'Lacota','1645122955999','true');
INSERT INTO PATIENT (id,full_Name,contact_Number,email,adm_Date,doctor,member_No,medical_Aid_Member) VALUES (9,'Ian','19378517810','Phasellus.dapibus.quam@odio.ca',CURRENT_DATE,'Mia','1620100858299','true');
INSERT INTO PATIENT (id,full_Name,contact_Number,email,adm_Date,doctor,member_No,medical_Aid_Member) VALUES (10,'Ethan','16772407960','lacus@arcu.co.uk',CURRENT_DATE,'Lilah','1684032922599','true');
INSERT INTO PATIENT (id,full_Name,contact_Number,email,adm_Date,doctor,member_No,medical_Aid_Member) VALUES (11,'Barrett','18402130851','gravida.Praesent@arcu.ca',CURRENT_DATE,'Cherokee','1650022061799','true');
INSERT INTO PATIENT (id,full_Name,contact_Number,email,adm_Date,doctor,member_No,medical_Aid_Member) VALUES (12,'Carson','12578255150','augue.porttitor@vel.com',CURRENT_DATE,'Chava','1636032980399','true');
INSERT INTO PATIENT (id,full_Name,contact_Number,email,adm_Date,doctor,member_No,medical_Aid_Member) VALUES (13,'Hall','17845461022','tincidunt.aliquam@nonenimMauris.ca',CURRENT_DATE,'Hayley','1686031514099','true');
INSERT INTO PATIENT (id,full_Name,contact_Number,email,adm_Date,doctor,member_No,medical_Aid_Member) VALUES (14,'Dieter','15667689550','arcu.Vivamus.sit@feugiatplacerat.edu',CURRENT_DATE,'Brynn','1682020875999','true');
INSERT INTO PATIENT (id,full_Name,contact_Number,email,adm_Date,doctor,member_No,medical_Aid_Member) VALUES (15,'Oliver','16799664582','tortor.nibh.sit@Aliquam.com',CURRENT_DATE,'Mira','1630081106099','true');
INSERT INTO PATIENT (id,full_Name,contact_Number,email,adm_Date,doctor,member_No,medical_Aid_Member) VALUES (16,'Ryan','16065942398','vel@a.com',CURRENT_DATE,'Vera','1609011638499','true');
INSERT INTO PATIENT (id,full_Name,contact_Number,email,adm_Date,doctor,member_No,medical_Aid_Member) VALUES (17,'Michael','17752467038','Proin.dolor.Nulla@velvenenatisvel.ca',CURRENT_DATE,'Penelope','1601081123799','true');
INSERT INTO PATIENT (id,full_Name,contact_Number,email,adm_Date,doctor,member_No,medical_Aid_Member) VALUES (18,'Branden','11857970362','non.nisi@quislectusNullam.edu',CURRENT_DATE,'Marah','1645100119699','true');
INSERT INTO PATIENT (id,full_Name,contact_Number,email,adm_Date,doctor,member_No,medical_Aid_Member) VALUES (19,'Kennedy','11916516184','vestibulum.nec.euismod@tempusscelerisquelorem.co.uk',CURRENT_DATE,'Ocean','1626111417099','true');
