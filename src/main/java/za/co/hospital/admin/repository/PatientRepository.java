package za.co.hospital.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import za.co.hospital.admin.model.Patient;

import java.util.List;

public interface PatientRepository  extends JpaRepository<Patient, Long> {
    List<Patient> findAll();

    Patient findById(Long patientId);
}
