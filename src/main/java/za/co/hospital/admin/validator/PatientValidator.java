package za.co.hospital.admin.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import za.co.hospital.admin.model.Patient;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class PatientValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return Patient.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Patient patient = (Patient) o;

        if(!Pattern.matches("[a-zA-Z ]+", patient.getFullName()))
        {
            errors.rejectValue("fullName", "TextOnly.admissionForm.fullName");
        }


        if(!validateEmail(patient.getEmail()))
        {
            errors.rejectValue("email", "Toplevel.admissionForm.email");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "doctor", "NotEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "admDate", "NotEmpty");
    }

    private boolean validateEmail(String emailStr) {
        final Pattern VALID_EMAIL_ADDRESS_REGEX =
                Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailStr);
        return matcher.find();
    }
}
