package za.co.hospital.admin.web;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import za.co.hospital.admin.model.Patient;
import za.co.hospital.admin.model.User;
import za.co.hospital.admin.service.PatientService;
import za.co.hospital.admin.service.SecurityService;
import za.co.hospital.admin.service.UserService;
import za.co.hospital.admin.validator.PatientValidator;
import za.co.hospital.admin.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private PatientService patientService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private PatientValidator patientValidator;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    @RequestMapping(value = "/admission", method = RequestMethod.GET)
    public String admission(Model model) {
        List<Patient> patientList = patientService.findAll();
        model.addAttribute("admissionForm", new Patient());
        model.addAttribute("patientList", patientList);

        return "admission";
    }

    @RequestMapping(value = "/admission/{patientId}", method = RequestMethod.GET)
    public String admission(Model model, @PathVariable("patientId") Long patientId) {
        Patient patient = patientService.findById(patientId);
        model.addAttribute("admissionForm", patient);

        return "patient";
    }


    @RequestMapping(value = "/admission", method = RequestMethod.POST)
    public String admission(@ModelAttribute("admissionForm") Patient admissionForm, BindingResult bindingResult, Model model) {
        patientValidator.validate(admissionForm, bindingResult);
        patientService.save(admissionForm);

        if (bindingResult.hasErrors()) {
/*            if (admissionForm.getAdmDate() == null){
                admissionForm.setAdmDate(new Date());
            }*/
            return "admission";
        }
        return welcome(model);
    }


    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(Model model) {
        model.addAttribute("userForm", new User());

        return "registration";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@ModelAttribute("userForm") User userForm, BindingResult bindingResult, Model model) {
        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return "registration";
        }

        userService.save(userForm);

        securityService.autologin(userForm.getUsername(), userForm.getPasswordConfirm());

        return "redirect:/welcome";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");

        return "login";
    }

    @RequestMapping(value = {"/", "/welcome"}, method = RequestMethod.GET)
    public String welcome(Model model) {
        List<Patient> patientList = patientService.findAll();
        model.addAttribute("admissionForm", new Patient());
        model.addAttribute("patientList", patientList);

        return "welcome";
    }
}
