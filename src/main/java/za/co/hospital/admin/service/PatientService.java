package za.co.hospital.admin.service;

import za.co.hospital.admin.model.Patient;

import java.util.List;

public interface PatientService {
    void save(Patient patient);

    List<Patient> findAll();

    Patient findById(Long patientId);
}
