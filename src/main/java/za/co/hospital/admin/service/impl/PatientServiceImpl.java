package za.co.hospital.admin.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.hospital.admin.model.Patient;
import za.co.hospital.admin.repository.PatientRepository;
import za.co.hospital.admin.service.PatientService;

import java.util.List;

@Service
public class PatientServiceImpl implements PatientService {

    @Autowired
    private PatientRepository patientRepository;

    @Override
    public void save(Patient patient) {
        patientRepository.save(patient);
    }

    @Override
    public List<Patient> findAll(){
       return patientRepository.findAll();
    }

    @Override
    public Patient findById(Long patientId){
        return patientRepository.findById(patientId);
    }
}
