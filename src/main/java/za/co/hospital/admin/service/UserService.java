package za.co.hospital.admin.service;

import za.co.hospital.admin.model.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
}
