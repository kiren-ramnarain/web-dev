<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<link href="${contextPath}/resources/thirdparty/fontawesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Create an account</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">

</head>
<body>
<div class="container">

    <c:if test="${pageContext.request.userPrincipal.name != null}">
        <form id="logoutForm" method="POST" action="${contextPath}/logout">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>

        <h2>Welcome user :${pageContext.request.userPrincipal.name} | <a
                onclick="document.forms['logoutForm'].submit()">Logout</a>
        </h2>


        <c:set var="admissionForm" value="${admissionForm}"/>
        <jsp:include page="admission.jsp">
            <jsp:param name="admissionForm" value="${admissionForm}"/>
        </jsp:include>

        <c:set var="patientList" value="${patientList}"/>
        <c:if test="${not empty patientList}">
            <jsp:include page="patientList.jsp">
                <jsp:param name="patientList" value="${patientList}"/>
            </jsp:include>
        </c:if>
    </c:if>

</div>
</body>
</html>
