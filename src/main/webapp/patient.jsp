<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="dateFormat" value="yyyy-MM-dd" scope="request"/>

<script type="text/javascript">
    $(document).ready(function () {
        var date_input = $('input[name="admDate"]'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
        var startDate = new Date(new Date().getFullYear(), 0, 1);
        var endDate = new Date(new Date().getFullYear(), 11, 31);
        console.log(startDate);
        console.log(endDate);
        var options = {
            format: 'yyyy-mm-dd',
            container: container,
            todayHighlight: true,
            autoclose: true,
            startDate: startDate,
            endDate: endDate
        };
        date_input.datepicker(options);

        function autocomplete(inp, arr) {
            /*the autocomplete function takes two arguments,
            the text field element and an array of possible autocompleted values:*/
            var currentFocus;
            /*execute a function when someone writes in the text field:*/
            inp.addEventListener("input", function (e) {
                var a, b, i, val = this.value;
                /*close any already open lists of autocompleted values*/
                closeAllLists();
                if (!val) {
                    return false;
                }
                currentFocus = -1;
                /*create a DIV element that will contain the items (values):*/
                a = document.createElement("DIV");
                a.setAttribute("id", this.id + "autocomplete-list");
                a.setAttribute("class", "autocomplete-items");
                /*append the DIV element as a child of the autocomplete container:*/
                this.parentNode.appendChild(a);
                /*for each item in the array...*/
                for (i = 0; i < arr.length; i++) {
                    /*check if the item starts with the same letters as the text field value:*/
                    if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                        /*create a DIV element for each matching element:*/
                        b = document.createElement("DIV");
                        /*make the matching letters bold:*/
                        b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                        b.innerHTML += arr[i].substr(val.length);
                        /*insert a input field that will hold the current array item's value:*/
                        b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                        /*execute a function when someone clicks on the item value (DIV element):*/
                        b.addEventListener("click", function (e) {
                            /*insert the value for the autocomplete text field:*/
                            inp.value = this.getElementsByTagName("input")[0].value;
                            /*close the list of autocompleted values,
                            (or any other open lists of autocompleted values:*/
                            closeAllLists();
                        });
                        a.appendChild(b);
                    }
                }
            });
            /*execute a function presses a key on the keyboard:*/
            inp.addEventListener("keydown", function (e) {
                var x = document.getElementById(this.id + "autocomplete-list");
                if (x) x = x.getElementsByTagName("div");
                if (e.keyCode == 40) {
                    /*If the arrow DOWN key is pressed,
                    increase the currentFocus variable:*/
                    currentFocus++;
                    /*and and make the current item more visible:*/
                    addActive(x);
                } else if (e.keyCode == 38) { //up
                    /*If the arrow UP key is pressed,
                    decrease the currentFocus variable:*/
                    currentFocus--;
                    /*and and make the current item more visible:*/
                    addActive(x);
                } else if (e.keyCode == 13) {
                    /*If the ENTER key is pressed, prevent the form from being submitted,*/
                    e.preventDefault();
                    if (currentFocus > -1) {
                        /*and simulate a click on the "active" item:*/
                        if (x) x[currentFocus].click();
                    }
                }
            });

            function addActive(x) {
                /*a function to classify an item as "active":*/
                if (!x) return false;
                /*start by removing the "active" class on all items:*/
                removeActive(x);
                if (currentFocus >= x.length) currentFocus = 0;
                if (currentFocus < 0) currentFocus = (x.length - 1);
                /*add class "autocomplete-active":*/
                x[currentFocus].classList.add("autocomplete-active");
            }

            function removeActive(x) {
                /*a function to remove the "active" class from all autocomplete items:*/
                for (var i = 0; i < x.length; i++) {
                    x[i].classList.remove("autocomplete-active");
                }
            }

            function closeAllLists(elmnt) {
                /*close all autocomplete lists in the document,
                except the one passed as an argument:*/
                var x = document.getElementsByClassName("autocomplete-items");
                for (var i = 0; i < x.length; i++) {
                    if (elmnt != x[i] && elmnt != inp) {
                        x[i].parentNode.removeChild(x[i]);
                    }
                }
            }

            /*execute a function when someone clicks in the document:*/
            document.addEventListener("click", function (e) {
                closeAllLists(e.target);
            });
        }

        autocomplete(document.getElementById("doctor"), doctors);

    });
</script>


<form:form autocomplete="off" method="POST" action="${contextPath}/admission" modelAttribute="admissionForm"
           class="form-signin">
    <h2 class="form-signin-heading">Hospital Admission</h2>

    <spring:bind path="id">
        <div class="form-group">
            <form:input id="id" type="hidden" path="id" class="form-control" placeholder="id"></form:input>
        </div>
    </spring:bind>

    <spring:bind path="fullName">
        <div class="form-group ${status.error ? 'has-error' : ''}">
            <label for="fullName">Full Name* : </label>
            <form:input id="fullName" type="text" path="fullName" class="form-control" placeholder="Enter name"
                        autofocus="true"></form:input>
            <form:errors path="fullName"></form:errors>
        </div>
    </spring:bind>

    <spring:bind path="contactNumber">
        <div class="form-group ${status.error ? 'has-error' : ''}">
            <label for="contactNumber">Contact number* : </label>
            <form:input id="contactNumber" type="number" path="contactNumber" class="form-control"
                        placeholder="Enter contact number"></form:input>
            <form:errors path="contactNumber"></form:errors>
        </div>
    </spring:bind>

    <spring:bind path="email">
        <div class="form-group ${status.error ? 'has-error' : ''}">
            <label for="email">Email* : </label>
            <form:input id="email" type="text" path="email" class="form-control"
                        placeholder="Enter email"></form:input>
            <form:errors path="email"></form:errors>
        </div>
    </spring:bind>

    <spring:bind path="medicalAidMember">
        <div class="form-group ${status.error ? 'has-error' : ''}">
            <label for="medical-aid-member">Medical aid member?</label>
            <form:checkbox id="medical-aid-member" path="medicalAidMember" class="checkbox"
                           value="${medicalAidMember}" onchange="showMemberNumber()"/>
        </div>
    </spring:bind>


    <div id="div-mem-no"
         <c:if test="${not medicalAidMember}">hidden</c:if>>
        <spring:bind path="memberNo">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label for="memberNo">Member number* : </label>
                <form:input id="member-no" type="number" path="memberNo" class="form-control"
                            placeholder="Enter member number"></form:input>
                <form:errors path="memberNo"></form:errors>
            </div>
        </spring:bind>
    </div>


    <spring:bind path="admDate">
        <fmt:formatDate var="admFormattedDate" pattern="${dateFormat}" value="${admissionForm.admDate}"/>
        <div class="form-group ${status.error ? 'has-error' : ''}">
            <label for="admDate">Admission date* : </label>
            <form:input type="text" id="admDate" path="admDate" value="${admFormattedDate}"  class="form-control"
                        placeholder="Enter admission date" readonly="true"></form:input>
            <form:errors path="admDate"></form:errors>
        </div>
    </spring:bind>

    <spring:bind path="doctor">
        <div class="autocomplete">
            <label for="email">Doctor* : </label>
            <form:input id="doctor" type="text" path="doctor" class="form-control"
                        placeholder="Doctor"></form:input>
            <form:errors path="doctor"></form:errors>
        </div>
    </spring:bind>

    <br>
    <br>
    <div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
    </div>
</form:form>
