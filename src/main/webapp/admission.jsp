<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admission</title>

    <!--  jQuery -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

    <!-- Isolated Version of Bootstrap, not needed if your site already uses Bootstrap -->
    <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css"/>

    <!-- Bootstrap Date-Picker Plugin -->
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

    <script src="${contextPath}/resources/js/jquery.min.js"></script>
    <script src="${contextPath}/resources/js/bootstrap.min.js"></script>
    <%--    <script src="${contextPath}/resources/js/bootstrap-datetimepicker.js"></script>--%>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <%--    <link href="${contextPath}/resources/css/bootstrap-datetimepicker.css" rel="stylesheet">--%>

    <!-- Bootstrap Date-Picker Plugin -->
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>


    <script type="text/javascript">

        var doctors = [
            "Doctor Savage",
            "Doctor Norton",
            "Doctor Ayala",
            "Gareth Stout",
            "Rudyard Snow",
            "Charles Taylor",
            "Buckminster Garrison",
            "Steven Coleman",
            "Yardley Workman",
            "Vance Montgomery",
            "Amos Strong",
            "Orson Tyson",
            "Quinlan Compton",
            "Grant Massey",
            "Rudyard Burris",
            "Melvin Lancaster",
            "Lucian Merrill",
            "Nehru Barrera",
            "Brian Bowman",
            "Rajah Murphy",
            "Arden Thornton",
            "Herman Fields",
            "Ashton Harrington",
            "Gary Graham",
            "Dylan Leonard",
            "Colby Murphy",
            "Jonah Holcomb",
            "Jerry Gallagher",
            "Garrett Juarez",
            "Griffin Howell",
            "Francis Huff",
            "Brenden Foreman",
            "Marvin Berg",
            "Shad Nash",
            "Brian Duke",
            "Graiden Lara",
            "Knox Rodriguez",
            "Dylan Espinoza",
            "Edan Mullins",
            "Price Mckinney",
            "Gregory Osborn",
            "Maxwell Wilson",
            "Jermaine Mullins",
            "Carlos Nolan",
            "Fuller Garza",
            "Nero Klein",
            "Ivor Austin",
            "Abraham Molina",
            "Talon Holmes",
            "Perry Holloway",
            "Martin Emerson",
            "Marvin Sharpe",
            "Thor Mccormick",
            "Ezra Sloan",
            "Timothy Sparks",
            "Zahir Durham",
            "Howard Solis",
            "Ross Kerr",
            "Malik Howe",
            "Finn Bowman",
            "Christian Branch",
            "Eagan Cabrera",
            "Beck Parker",
            "Nathaniel Burks",
            "Mason Bennett",
            "Orson Perkins",
            "Arthur Sandoval",
            "Alec Arnold",
            "Garth Yates",
            "Benjamin Rowe",
            "Matthew Delacruz",
            "Russell Ochoa",
            "Harper Sims",
            "Chaim Briggs",
            "Silas Tyson",
            "Keaton Jennings",
            "Harding Gonzales",
            "Calvin Beach",
            "Bert Graham",
            "Graham Sherman",
            "Ethan Mckenzie",
            "Berk Hardy",
            "Omar Kent",
            "Hayden Reilly",
            "Brenden Baker",
            "Simon Durham",
            "Eagan Zimmerman",
            "Chancellor Stevenson",
            "Quinn Gates",
            "Fletcher Hayes",
            "Aaron Mcgee",
            "Caleb Booker",
            "Norman Olson",
            "Driscoll Small",
            "Ivan Scott",
            "Leroy Mosley",
            "Nasim Thompson",
            "Kennan Ballard",
            "Lawrence Serrano",
            "Malik Kline"
        ];


        function showMemberNumber() {
            var showDisplay = document.getElementById("div-mem-no");
            if (document.getElementById("medical-aid-member").checked == true) {
                console.log("show member no");
                showDisplay.style.display = "block";
            } else {
                console.log("hide member no");
                showDisplay.style.display = "none";
            }

        }

        function editPatient(row) {
            var patientId = $(row).data("patientid");
            //url ="${contextPath}/admission/"+patientId;
            console.log(${contextPath});
            url = "http://localhost:8080/admission/"+patientId;

            console.log(url);

            $.ajax({
                url: url,
                type: "GET",
                success: function (result) {
                    $("#patientContainer").html(result);
                    $('html, body').animate({ scrollTop: 0 }, 'fast');
                },
                error: function (error) {
                    console.log(error);
                }

            })

        }




    </script>


</head>

<body>

<div class="container">
    <sec:authorize access="hasAuthority('ADMIN')">
        <c:set var="admissionForm" value="${admissionForm}"/>
        <div id="patientContainer">
            <jsp:include page="patient.jsp">
                <jsp:param name="admissionForm" value="${admissionForm}"/>
            </jsp:include>
        </div>
    </sec:authorize>
    <br>
    <br>


</div>


</body>
</html>
