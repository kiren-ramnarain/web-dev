<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<c:set var="dateFormat" value="yyyy-MM-dd" scope="request"/>

<table class="table table-bordered">
    <thead>
    <tr class="panel-primary-header">
        <th>id</th>
        <th>Full name</th>
        <th>Contact Number</th>
        <th>Email</th>
        <th>Admission Date</th>
        <th>Doctor</th>
        <th>Member Number</th>
        <th>Medical Aid Member</th>


    </tr>
    </thead>
    <tbody>
    <c:forEach var="pat" items="${patientList}" varStatus="index">
        <fmt:formatDate var="admFormattedDate" pattern="${dateFormat}" value="${pat.admDate}"/>
        <tr>
            <td><c:out value="${pat.id}"/></td>
            <td><c:out value="${pat.fullName}"/></td>
            <td><c:out value="${pat.contactNumber}"/></td>
            <td><c:out value="${pat.email}"/></td>
            <td><c:out value="${admFormattedDate}"/></td>
            <td><c:out value="${pat.doctor}"/></td>
            <td><c:out value="${pat.memberNo}"/></td>
            <c:choose>
                <c:when test="${ pat.medicalAidMember}">
                    <td><c:out value="Yes"/></td>
                </c:when>
                <c:otherwise>
                    <td><c:out value="No"/></td>
                </c:otherwise>
            </c:choose>
            <sec:authorize access="hasAuthority('ADMIN')">
            <td>
                <button class="btn btn-sm btn-primary btn-block" type="button" data-patientid="${pat.id}"
                        onclick="editPatient(this);">Edit
                </button>
            </td>
            <td>
                <button class="btn btn-sm btn-primary btn-block" type="button">Delete</button>
            </td>
            </sec:authorize>
        </tr>
    </c:forEach>
    </tbody>
</table>