## Prerequisites
- JDK 1.7 or later
- Maven 3 or later

## Stack
- Spring Security
- Spring Boot
- Spring Data JPA
- Maven
- JSP
- HSQL

## Run
-mvn clean spring-boot:run

## url 
http://localhost:8080/login


##Test Admin User
admin
123

##Test Doctor User
paday
123
